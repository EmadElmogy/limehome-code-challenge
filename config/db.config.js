/* eslint-disable no-console */
const dotenv = require('dotenv');
dotenv.config();

let options = {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    charset: 'utf8',
    omitNull: false,
    alter: true,
    define: {
        collate: 'utf8mb4_unicode_ci',
    }, pool: {
        max: 50,
        min: 0,
        idle: 500,
        acquire: 10000,
        evict: 3000,
        handleDisconnects: true
    },
    dialectOptions: {
        bigNumberStrings: false,
        decimalNumbers: true,
				useUTC: true,
				timezone: 'Etc/GMT0',
    },
    timestamps: true,
		timezone: 'Etc/GMT0',
    logging: console.log
};

module.exports = {
	production: options,
	staging: options,
	development: options
};
