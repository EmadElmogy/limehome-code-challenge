const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const dotenv = require('dotenv');
dotenv.config();
// Swagger definition
const swaggerDefinition = {
  info: {
    title: 'REST API for LimeHome Code Challenge', // Title of the documentation
    version: '1.0.0', // Version of the app
    description: 'This is the REST API for LimeHome Code Challenge', // short description of the app
  },
  host: `localhost:${process.env.APP_PORT}`, // the host or url of the app
  basePath: '' // the basepath of your endpoint
  // securityDefinitions: {
  //   Bearer: {
  //     type: "apiKey",
  //     in: "header",
  //     name: "Authorization",
  //     description: "user access token"
  //   }
  // }
};
// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ['./routes-limehome-challenge/*.doc.yaml'],
};
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);
function addSwagger(app){
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
}
module.exports = {
  addSwagger
};
