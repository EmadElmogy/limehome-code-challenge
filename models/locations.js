module.exports = (sequelize, DataTypes) => {
  const locations = sequelize.define('locations', {
    name: DataTypes.STRING,
    latitude: DataTypes.FLOAT(10, 8),
    longitude: DataTypes.FLOAT(10, 8),
    status: DataTypes.ENUM('active', 'inactive')
  }, {
    hooks: {
      beforeCount (options) {
        options.raw = true;
      }
    },
    paranoid: true,
    timestamps: true,
    underscored: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });

  // eslint-disable-next-line no-unused-vars
  locations.associate = (models) =>  {
  };

  return locations;
};
