'use strict';
const express = require('express');
const router = express.Router();
const controller = require ('./locations.controller') ;

/**
 * @api {get} /locations List all locations
 * @apiGroup Locations
 */
router.get('/locations', controller.getLocations);


module.exports = router;
