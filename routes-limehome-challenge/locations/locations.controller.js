const formatResponse = require('../../helpers/response.helper').formatResponse;
const service = require('./locations.service');
const HttpResponses = require('./../../constants/http-responses.constants');


async function getLocations(req, res, next) {
  try {
    let locations = await service.getLocations({ status: 'active' });
    return res.status(HttpResponses.OK.code).json(formatResponse(locations));
  } catch (e) {
    return next(e);
  }
}


module.exports = {
  getLocations
};
