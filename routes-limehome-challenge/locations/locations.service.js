const models = require('./../../models');
const Locations = models.locations;

async function getLocations(whereConditions) {
  return Locations.findAll({ where: whereConditions });
}

async function getLocation(whereConditions) {
  return Locations.findOne({ where: whereConditions });
}

module.exports = {
  getLocations,
  getLocation
};
