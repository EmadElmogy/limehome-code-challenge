const formatResponse = require('../../helpers/response.helper').formatResponse;
const service = require('./hotels.service');
const HttpResponses = require('./../../constants/http-responses.constants');

async function getHotels(req, res, next) {
  try {
    let location = await service.getLocation({ id: req.params.id, status: 'active' });
    let nearHotels = await service.getHotels(location);
    return res.status(HttpResponses.OK.code).json(formatResponse(nearHotels));
  } catch (e) {
    return next(e);
  }
}


module.exports = {
  getHotels
};
