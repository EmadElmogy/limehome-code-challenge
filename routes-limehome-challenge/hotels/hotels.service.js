const models = require('./../../models');
const Locations = models.locations;
const axios = require('axios');

async function getLocation(whereConditions) {
  return Locations.findOne({ where: whereConditions });
}

async function getHotels(location) {
  return axios({
    method: 'GET',
    url: `https://places.demo.api.here.com/places/v1/discover/here?app_id=SuEIY2one3enuIbzkLfm&app_code=Lo91n62j_ibQicDiw-euGA&at=${location.latitude},${location.longitude};r=150&cat=hotel`,
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(result => {
    return result.data.results.items;
  }).catch(e => {
    return e;
  });
}

module.exports = {
  getLocation,
  getHotels
};
