'use strict';
const express = require('express');
const router = express.Router();
const controller = require ('./hotels.controller') ;

router.get('/hotels/:id', controller.getHotels);


module.exports = router;
