const app = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');

const { expect } = chai;
chai.use(chaiHttp);

describe('/GET hotels', () => {
  it('it should get all near by hotels', done => {
    chai
      .request(app)
      .get('/hotels/1')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.status).to.equals('OK');
        done();
      });
  });
});
