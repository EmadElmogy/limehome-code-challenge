const app = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');

const { expect } = chai;
chai.use(chaiHttp);

describe('/GET locations', () => {
  it('it should Get all locations', done => {
    chai
      .request(app)
      .get('/locations')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.status).to.equals('OK');
        done();
      });
  });
});
