'use strict';

let models = require('./../models');

let locations = [
	{
		id:1,
		name:'first location',
		latitude:53.2711,
		longitude:-9.0541,
		status:'active'
	},
	{
		id:2,
		name:'second location',
		latitude:37.7942,
		longitude:-122.4070,
		status:'active'
	}
];


module.exports = {
    up: () => {
        let promises = locations.map(location => {
            return models.locations
							.findOrCreate({
								where: {id: location.id},
								defaults: location,
								paranoid: true})
							.spread((result, created)=> {
								if (!created)
									return result.update(location);

								return result;
							});
        });

        return Promise.all(promises);
    },

    down: () => {
        let promises = locations.map(location => {
            return models.locations
							.destroy({
								where: {id: location.id},
								paranoid: false,
								truncate: true,
								cascade: false});
        });

        return Promise.all(promises);
    }
};
