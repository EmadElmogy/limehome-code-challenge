const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');

const winston = require('./middleware/winston.middleware');

require('./models');

const limeHomeRoutes = require('./routes-limehome-challenge/limehome.routes');
const formatResponse = require('./helpers/response.helper').formatResponse;

let app = express();

dotenv.config();

// eslint-disable-next-line no-undef
app.set('config', __config);

// global scopes
global.__baseDir = './';

// Swagger
const swagger = require('./limeHome.swagger');
swagger.addSwagger(app);

// CORS
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
limeHomeRoutes.setRoutes(app);
app.use(createLogs);
app.use(handleNotFound);
app.use(handleErrors);

module.exports = app;

function handleErrors(err, req, res, next) {
	let ip = req.connection.remoteAddress;
	let statusCode = err.status || 500;
	let statusMessage = err.statusCode || 'Internal Server Error';
	winston.error({
		address: ip,
		path: req.path,
		params: req.params,
		query: req.query,
		statusCode: statusCode,
		statusMessage: statusMessage,
		errorMessage: err.message,
		SequelizeForeignKeyConstraintError: err.SequelizeForeignKeyConstraintError
	});
	if (err.transaction)
		err.transaction.rollback();

	res.status(statusCode).json(formatResponse(null, statusCode, statusMessage, err.message));
	next();
}

function handleNotFound(req, res, next) {

	let ip = req.connection.remoteAddress;
	let statusCode = 404;
	let statusMessage = 'NOT FOUND';
	winston.error({
		address: ip,
		path: req.path,
		params: req.params,
		query: req.query,
		statusCode: statusCode,
		errorMessage: statusMessage,
	});
	res.status(statusCode).json(formatResponse(null, statusCode, statusMessage, statusMessage));
	next();
}

function createLogs(req, res, next) {
	let ip = req.connection.remoteAddress;
	winston.info({
		address: ip,
		path: req.path,
		params: req.params,
		query: req.query
	});
	next();
}
